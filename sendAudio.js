const { Kafka } = require('kafkajs')
let config = require('./config')
var fs = require('fs');

let path = require('path')
let audio_path = path.resolve(process.cwd(), 'test-audio.mp3')

let index = 0;

let topic = config.topic

const kafka = new Kafka({
    clientId: 'my-app',
    brokers: config.brokers,
    authenticationTimeout: 45000,
    ssl: config.ssl,
    sasl: {
        mechanism: config.authMechanism, // scram-sha-256 or scram-sha-512
        username: config.username,
        password: config.password
    },

})
const producer = kafka.producer()

async function main() {

    await producer.connect()
    console.log("Connected to Kafka")
    console.log("Sending message to Kafka")
    console.log("Sending to topic: " + topic)

    var audio_data = fs.readFileSync(audio_path);
    console.log(audio_data)
    const CHUNK_SIZE = 100000; // 100 KB

    // Inform of start of sending to consumer
    await producer.send({
        topic,
        messages: [
            {
                key: "parseId", value: JSON.stringify({
                    parseId: "parseId",
                    start: true
                })
            },
        ],
    })

    for (let bytesRead = 0; bytesRead < audio_data.length; bytesRead = bytesRead + CHUNK_SIZE) {
        console.log(index + " chunk sent")
        let audio_data_chunk = audio_data.slice(bytesRead, bytesRead + CHUNK_SIZE)

        await producer.send({
            topic,
            messages: [
                {
                    key: "parseId", value: JSON.stringify({
                        index,
                        audio_data_chunk: audio_data_chunk,
                        timestamp: (new Date()).getTime(),
                        parseId: "parseId",
                        progress: true
                    })
                },
            ],
        })
        index++

    }

    // inform of end so that processing starts
    await producer.send({
        topic,
        messages: [
            {
                key: "parseId", value: JSON.stringify({
                    parseId: "parseId",
                    end: true
                })
            },
        ],
    })

    await producer.disconnect()
    console.log("Disconnected from Kafka")

}

if(require.main === module){
    main();
}