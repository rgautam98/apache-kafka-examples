let config = {
    topic: "messages",
    brokers: ["localhost:9092"],
    username: "username",
    password: "password",
    authMechanism: "plain", // plain, scram-sha-256 or scram-sha-512
    ssl: true
}

module.exports = config
