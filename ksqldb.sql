-- If you are using ksqldb cli, first create a stream to consume data from the topics
  
  CREATE STREAM messages
  (
    id bigint,
    name string,
    index bigint
  )
  WITH (KAFKA_TOPIC='messages', VALUE_FORMAT='JSON');

CREATE TABLE messtable (
    id string,
    name string,
    index string PRIMARY KEY
   ) WITH (
     KAFKA_TOPIC = 'messages', 
     VALUE_FORMAT = 'JSON'
   );


select * from messages emit CHANGES;