const { Kafka } = require('kafkajs')
let config = require('./config')
var fs = require('fs');

let topic = config.topic
let fromBeginning = false
/*
 If this is true and autocommit is set to true, then you will read all the messages only once. 
 autoCommit = true means to read from beginning, resetOffsets should be true
*/
let resetOffsets = false;

let output_file = 'stream-output.mp3'

const kafka = new Kafka({
    clientId: 'another-app',
    brokers: config.brokers,
    authenticationTimeout: 45000,
    ssl: config.ssl,
    sasl: {
        mechanism: config.authMechanism, // scram-sha-256 or scram-sha-512
        username: config.username,
        password: config.password
    },
})


const consumer = kafka.consumer({ groupId: 'test-group' })

// Application specific varaiables
let audio_chunks = {}


async function handleAudioChunks(message){
    if(message.start){
        console.log("Started recording audio for parseId: " + message.parseId)
        audio_chunks[message.parseId] = []
        console.log(audio_chunks)
    } else if (message.progress){
        // Save the chunks to audio_chunks[message.parseId]
        if(!audio_chunks[message.parseId]){
            console.log("Started recording audio ")
            audio_chunks[message.parseId] = []
        }
        audio_chunks[message.parseId].push({
            index: message.index,
            audio_data_chunk: message.audio_data_chunk,
            timestamp: message.timestamp
        })
        console.log("Saved chunk: " + message.index + " ")
    } else if (message.end){
        console.log("Ended recording audio ")
        // sort chunks by index
        audio_chunks[message.parseId].sort((a, b) => a.index - b.index)
        // save the chunks to a file
        for(let i of audio_chunks[message.parseId]){
            fs.appendFileSync(output_file, Buffer.from(i.audio_data_chunk), {
                encoding: 'binary'
            })
        }
        audio_chunks[message.parseId] = null
        console.log("Saved audio chunks to file: " + output_file)
    }
}

async function main(){
    await consumer.connect()
    console.log("Connected to Kafka")
    await consumer.subscribe({ topics: [topic], fromBeginning })
    console.log("Subscribed to topic")

    await consumer.run({
        autoCommit: true,
        eachMessage: async (msgObj) => {
            // console.log("The message object is")
            // console.log(msgObj)
            // there are heartbeat and pause functions in msgObj
            // console.log("The message is")
            // console.log({
            //     partition: msgObj.partition,
            //     key: msgObj.message.key ? msgObj.message.key.toString(): "",
            //     value: msgObj.message.value.toString(),
            //     offset: msgObj.message.offset,
            //     headers: msgObj.message.headers,
            // })

            console.log("Message Received")

            // msgObj.heartbeat()
            // If your process is taking time. Else the message will be delivered to another consumer
            handleAudioChunks(JSON.parse(msgObj.message.value.toString()))
        },
    })

    if(resetOffsets){
        consumer.commitOffsets([
            { topic: topic, partition: 0, offset: '0' },
        ])
    }
}

main()